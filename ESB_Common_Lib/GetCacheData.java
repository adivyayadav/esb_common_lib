package com.clix.capital.esb;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.ibm.broker.plugin.MbElement;
import com.ibm.broker.plugin.MbGlobalMap;

public class GetCacheData {
	public static Boolean loadGlobalCacheByGroupName(String groupName, MbElement envRef) {
		Boolean result = false;
		try {					
			Map<String, String> configurationPropertiesData = getMapByGroup(groupName.toUpperCase());
			if (configurationPropertiesData != null) {
				MbElement configurationPropertiesEle = envRef
						.createElementAsLastChild(MbElement.TYPE_NAME,
								"configurationProperties", "");
				Set<String> configPropsKeys = configurationPropertiesData
						.keySet();
				Iterator<String> i = configPropsKeys.iterator();
				String key;	
				while (i.hasNext()) {
					key = i.next();
					configurationPropertiesEle.createElementAsLastChild(
							MbElement.TYPE_NAME, key.toUpperCase(),
							configurationPropertiesData.get(key));					
				}
				result = true;
			}

		} catch (Exception e) {

		}
		return result;
	}

	public static String getGlobalCacheValueByPropertyName(String groupName, String propName) {
		String result = null;
		groupName=groupName.toUpperCase();
		propName=propName.toUpperCase();
		try {
			Map<String, String> configurationPropertiesData = getMapByGroup(groupName);
			if (configurationPropertiesData != null) {
				result = configurationPropertiesData.get(propName);
			}
		} catch (Exception e) {

		}
		return result;
	}

	private static Map<String, String> getMapByGroup(String groupName) {
		Map<String, String> configurationPropertiesData = null;
		try {
			MbGlobalMap globalMap = MbGlobalMap.getGlobalMap("globalCache");
			Map<String, Map<String, String>> configurationProperties = (Map<String, Map<String, String>>) globalMap
					.get("configurationProperties");
			if (configurationProperties.containsKey(groupName)) {
				configurationPropertiesData = (Map<String, String>) configurationProperties
						.get(groupName);

			}
		} catch (Exception e) {

		}
		return configurationPropertiesData;
	}
}
