package com.clix.capital.esb;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ibm.broker.javacompute.MbJavaComputeNode;
import com.ibm.broker.plugin.MbElement;
import com.ibm.broker.plugin.MbException;
import com.ibm.broker.plugin.MbGlobalMap;
import com.ibm.broker.plugin.MbMessage;
import com.ibm.broker.plugin.MbMessageAssembly;
import com.ibm.broker.plugin.MbOutputTerminal;
import com.ibm.broker.plugin.MbUserException;
import com.ibm.broker.plugin.MbNode.JDBC_TransactionType;

public class LoadCache extends MbJavaComputeNode {
//	public void onInitialize()throws MbUserException{
//		try {
//		MbGlobalMap globalMap = MbGlobalMap.getGlobalMap("globalCache");
//		boolean _isCacheEmpty = globalMap.containsKey("isCacheEmpty");
//		if (!_isCacheEmpty) {
//			Connection conn = getJDBCType4Connection("CLXESBDB_DS",
//					JDBC_TransactionType.MB_TRANSACTION_AUTO);
//			// Example of using the Connection to create a
//			// java.sql.Statement
//			Statement stmt = conn.createStatement(
//					ResultSet.TYPE_SCROLL_INSENSITIVE,
//					ResultSet.CONCUR_READ_ONLY);
//			ResultSet services_RS = stmt
//					.executeQuery("SELECT GROUP_NAME,PROPERTY_NAME,PROPERTY_VALUE FROM ESB_PROPERTY_CONFIGURATIONS WHERE CACHE_SCOPE LIKE 'ALL' ORDER BY GROUP_NAME");
//			// ServiceDetailsData servData = null;
//			Map<String, Map<String, String>> serviceDetails = new HashMap<String, Map<String, String>>();
//			Map<String, String> serviceData = new HashMap<String, String>();
//			String groupName = "";
//			String altGroupName = null;
//			while (services_RS.next()) {
//				groupName = services_RS.getString("GROUP_NAME");
//				if (altGroupName == null) {
//					altGroupName = groupName;
//				}
//				if (!altGroupName.equals(groupName)) {
//					serviceDetails.put(altGroupName, serviceData);
//					altGroupName = groupName;
//					serviceData = new HashMap<String, String>();
//				}
//				serviceData.put(services_RS.getString("PROPERTY_NAME"),
//						services_RS.getString("PROPERTY_VALUE"));
//			}
//			serviceDetails.put(altGroupName, serviceData);
//			altGroupName = groupName;
//			serviceData = new HashMap<String, String>();
//			if (globalMap.containsKey("configurationProperties")) {
//				globalMap.update("configurationProperties", serviceDetails);
//				globalMap.update("isCacheEmpty", false);
//			} else {
//				globalMap.put("configurationProperties", serviceDetails);
//				globalMap.put("isCacheEmpty", false);
//			}
//		}
//		}catch(Exception e){
////			throw new MbUserException(this, "evaluate()", "123", "456", e.toString(),
////					null);
//		}
//	}
	public void evaluate(MbMessageAssembly inAssembly) throws MbException {
		MbOutputTerminal out = getOutputTerminal("out");
		MbOutputTerminal alt = getOutputTerminal("alternate");

		MbMessage inMessage = inAssembly.getMessage();
		MbMessageAssembly outAssembly = null;
		try {
			// create new message as a copy of the input
			MbMessage outMessage = new MbMessage(inMessage);
			outAssembly = new MbMessageAssembly(inAssembly, outMessage);
			// ----------------------------------------------------------
			// Add user code below
			Connection conn = getJDBCType4Connection("CLXESBDB_DS",
					JDBC_TransactionType.MB_TRANSACTION_AUTO);
			// Example of using the Connection to create a
			// java.sql.Statement
			Statement stmt1 = conn.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet services_RS1 = stmt1
					.executeQuery("SELECT PROPERTY_VALUE FROM ESB_PROPERTY_CONFIGURATIONS WHERE PROPERTY_NAME LIKE 'CONFIGURATION PROPERTY' AND CACHE_SCOPE LIKE 'ALL' ");
			while (services_RS1.next())
		      {       
		        boolean cache_flag = services_RS1.getBoolean("PROPERTY_VALUE");
				MbMessage env = inAssembly.getGlobalEnvironment();
				env.getRootElement().createElementAsFirstChild(MbElement.TYPE_NAME_VALUE, "Global", cache_flag);
			  }
			MbElement env = inAssembly.getGlobalEnvironment().getRootElement();
			MbElement storedcache_flag = env.getFirstElementByPath("Global"); 
			boolean flag_status = (boolean) storedcache_flag.getValue(); 
			if (flag_status == true)
			{
			   stmt1.executeUpdate("UPDATE ESB_PROPERTY_CONFIGURATIONS SET PROPERTY_VALUE = \"FALSE\" WHERE  GROUP_NAME= \"ESB_GLOBAL_CACHE\" AND PROPERTY_NAME = \"CONFIGURATION PROPERTY\"");
			}
			MbGlobalMap globalMap = MbGlobalMap.getGlobalMap("globalCache");
			boolean _isCacheEmpty = globalMap.containsKey("isCacheEmpty");
//			if (!_isCacheEmpty) {
			if (!_isCacheEmpty || flag_status == true ){
				/* */
				// Example of using the Connection to create a
				// java.sql.Statement
				Statement stmt = conn.createStatement(
						ResultSet.TYPE_SCROLL_INSENSITIVE,
						ResultSet.CONCUR_READ_ONLY);
				ResultSet services_RS = stmt
						.executeQuery("SELECT GROUP_NAME,PROPERTY_NAME,PROPERTY_VALUE FROM ESB_PROPERTY_CONFIGURATIONS WHERE CACHE_SCOPE LIKE 'ALL' ORDER BY GROUP_NAME");
				// ServiceDetailsData servData = null;
				Map<String, Map<String, String>> serviceDetails = new HashMap<String, Map<String, String>>();
				Map<String, String> serviceData = new HashMap<String, String>();
				String groupName = "";
				String altGroupName = null;
				while (services_RS.next()) {
					groupName = services_RS.getString("GROUP_NAME");
					if (altGroupName == null) {
						altGroupName = groupName;
					}
					if (!altGroupName.equals(groupName)) {
						serviceDetails.put(altGroupName, serviceData);
						altGroupName = groupName;
						serviceData = new HashMap<String, String>();
					}
					serviceData.put(services_RS.getString("PROPERTY_NAME"),
							services_RS.getString("PROPERTY_VALUE"));
				}
				serviceDetails.put(altGroupName, serviceData);
				altGroupName = groupName;
				serviceData = new HashMap<String, String>();
				if (globalMap.containsKey("configurationProperties")) {
					globalMap.update("configurationProperties", serviceDetails);
					globalMap.update("isCacheEmpty", false);
				} else {
					globalMap.put("configurationProperties", serviceDetails);
					globalMap.put("isCacheEmpty", false);
				}
			}
			// End of user code
			// ----------------------------------------------------------
		} catch (MbException e) {
			// Re-throw to allow Broker handling of MbException
			throw e;
		} catch (RuntimeException e) {
			// Re-throw to allow Broker handling of RuntimeException
			throw e;
		} catch (Exception e) {
			// Consider replacing Exception with type(s) thrown by user code
			// Example handling ensures all exceptions are re-thrown to be
			// handled in the flow
			throw new MbUserException(this, "evaluate()", "", "", e.toString(),
					null);
		}
		// The following should only be changed
		// if not propagating message to the 'out' terminal
		out.propagate(outAssembly);

	}
}
